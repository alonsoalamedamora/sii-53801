# Changelog
Todos los cambios que se produzcan en el juego se veran reflejado en este archivo.

## [1.6] - 26-12-2020
### Añadido:
- Comunicación mediante sockets entre el cliente y el servidor
### Borrado:
- Fifo de comunicación entre servidor y cliente para pasarle la posición de la bola.
- Fifo de comunicación entre cliente y servidor para pasarle la tecla pulsada.

## [1.5] - 15-12-2020
### Añadido:
- Añadido programa cliente y servidor.
- Fifo de comunicación entre servidor y cliente para compartir al cliente la posición de la bola, de las raquetas y los puntos del juego.
- Fifo e hilo entre cliente y servidor para compartir las teclas pulsadas por el cliente. 
### Borrado:
- Programa tenis y mundo de la versión v1.4.

## [1.4] - 2-12-2020
### Añadido:
- Programa logger que se comunica con el programa tenis mediante una fifo.
- Programa bot que controla la raqueta 1 y se comunica mediante proyección en memoria.
- Funcionalidad de finalizar el juego si alguno de los jugadores llega a 3 puntos.
- Funcionalidad de que el bot controle la raqueta 2 si el jugador lleva 10 segundos sin haber pulsado una tecla.
- Estructura DatosMemCompartida.h para realizar la proyección en memoria.

## [1.3] - 12-11-2020
### Añadido:  
- Cambio en el movimiento de la Esfera y las raquetas.
- Funcionalidad de cambiar el tamaño de la bola pasados 5 segundos.
- Fichero README con las instrucciones del juego

## [1.2] - 15-10-2020
### Añadido:
- Cambio en cabeceras


