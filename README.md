INSTRUCCIONES DEL JUEGO:

-Movimiento jugador 1: "w" y "s" para subir y bajar.
-Movimiento jugador 2: "o" y "l" para subir y bajar.
-El juego tiene un temporizador y pasados 5 segundos empezará a decrecer el tamaño de la esfera.
-En los marcadores de la parte de arriba se muestra la puntuación de cada jugador.
