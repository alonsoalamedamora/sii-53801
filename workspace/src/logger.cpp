#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <Puntos.h>
int main (){

	puntuaciones info;
	int fd;

	if(mkfifo("/tmp/logger",0777)<0){ 	 //Creacion fifo  
		perror("Creacion fifo");
		return 1;
	}

	if ((fd=open("/tmp/logger",O_RDONLY))<0){
		perror("Open fifo");
		return 1;
	}
	while(1){
		if(read(fd,&info,sizeof(info))==sizeof(info)){

			if(info.ganador==1){
				printf("Jugador 1 marca 1 punto, lleva un total de %d puntos. \n",info.puntos1);
			}
			
			if (info.ganador==2){
				printf("Jugador 2 marca 1 punto, lleva un total de %d puntos. \n",info.puntos2);	
			}
		}
		else if(read(fd,&info,sizeof(info))==0){
			return 0;
		}
	}
	close (fd);
	unlink("/tmp/logger");
	return 0;
}


