// Mundo.cpp: implementation of the CMundo class.
//Autor: Alonso Alameda Mora
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close (fd);
	unlink("/tmp/logger");
	//conexion.Close();
	//comunicacion_servidor.Close();
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{

	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos.puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d", puntos.puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
/*	if((puntos1==3)||(puntos2==3)){
		exit(1);
	}
*/	int flanco=0;
	time_t y=time(NULL);
	if (difftime(y,t)>=5)
		flanco=1;
	else if (difftime(y,t2)>=10)
		flanco2=1;
	else{
		flanco2=0;
		flanco=0;
	}
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f,flanco);
	int i;
	
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esfera.radio=0.5;
		t=time(NULL);
		puntos2++;
		puntos.ganador=2;
		puntos.puntos1=puntos1;
		puntos.puntos2=puntos2;
		write(fd,&puntos,sizeof(puntos));

	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.radio=0.5;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esfera.radio=0.5;
		t=time(NULL);
		puntos1++;
		puntos.ganador=1;
		puntos.puntos1=puntos1;
		puntos.puntos2=puntos2;
		write(fd,&puntos,sizeof(puntos));
		
	}	

	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d %f",esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2,esfera.radio);
	comunicacion_servidor.Send(cad,sizeof(cad));


}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{	
	flanco2=0;
}

void* hilo_comandos(void* d)
{
	CMundo* p=(CMundo*) d;
	p->RecibeComandosJugador();
}
void CMundo::RecibeComandosJugador()
{
	while(1){
		usleep(10);
		char cad[100];
		unsigned char key;
		comunicacion_servidor.Receive(cad,sizeof(cad));
		sscanf(cad,"%c",&key);
		if(key=='s')jugador1.velocidad.y=-4;
		else if(key=='w')jugador1.velocidad.y=4;
		else if(key=='l')jugador2.velocidad.y=-4;
		else if(key=='o')jugador2.velocidad.y=4;
		
	}
}
void CMundo::Init()
{
	////////////////////////////////////////////////////
	t=time(NULL);
	t2=time(NULL);
	Plano p;
	if ((fd=open("/tmp/logger",O_WRONLY))<0){
		perror("Abrir fifo logger");
	}

	//////////////////////////////////////
	pthread_create(&thid1,NULL,hilo_comandos,this);
	//////////////////////////////////////
	//////////////////////////////////////
	
	char ip[]="127.0.0.1";
	char nombre[30];
	if ((conexion.InitServer(ip,8000))==-1){printf("Error a inicializar el servidor\n");}
	else printf("Servidor creado\n");
	comunicacion_servidor=conexion.Accept();	
	if((comunicacion_servidor.Receive(nombre,sizeof(nombre)))<0){printf("Error al recibir el nombre del cliente");}
	else printf("%s ha entrado a la partida\n",nombre);
	
	//////////////////////////////////////
	//////////////////////////////////////
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}




