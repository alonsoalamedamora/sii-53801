// Esfera.cpp: implementation of the Esfera class.
//Autor: Alonso Alameda Mora
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	rojo=20;
	verde=218;
	azul=152;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(rojo,verde,azul);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t, int flanco)
{
centro.x=centro.x+velocidad.x*t;
centro.y=centro.y+velocidad.y*t;
rojo=rojo+3;
verde=verde+15;
azul=azul+51;
if (flanco){
	if(radio>0.15){
		radio=radio-0.0008;
	}
}
}
