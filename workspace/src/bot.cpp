#include <stdio.h>
#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

int main(){
	int fd;
	DatosMemCompartida *pdatos;
	if((fd=open("datos",O_RDWR))<0){
		perror("Open descriptor de fichero");
	}
	pdatos=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE,MAP_SHARED,fd,0));
	if (pdatos==MAP_FAILED){
		perror("Proyeccion en memoria");
		close (fd);
	}
	close (fd);
	while(1){
	if(pdatos->esfera.centro.y==((pdatos->raqueta1.y1+pdatos->raqueta1.y2)/2)){
		pdatos->accion=0;
	}
	else if(pdatos->esfera.centro.y<((pdatos->raqueta1.y1+pdatos->raqueta1.y2)/2)){
		pdatos->accion=-1;
	}
	else if(pdatos->esfera.centro.y>((pdatos->raqueta1.y1+pdatos->raqueta1.y2)/2)){
		pdatos->accion=1;
	}
	
	if(pdatos->esfera.centro.y==((pdatos->raqueta2.y1+pdatos->raqueta2.y2)/2)){
		pdatos->accion2=0;
	}
	else if(pdatos->esfera.centro.y<((pdatos->raqueta2.y1+pdatos->raqueta2.y2)/2)){
		pdatos->accion2=-1;
	}
	else if(pdatos->esfera.centro.y>((pdatos->raqueta2.y1+pdatos->raqueta2.y2)/2)){
		pdatos->accion2=1;
	}
	
	usleep(25000);
}
	munmap(pdatos,sizeof(DatosMemCompartida));
	unlink("datos");
}
